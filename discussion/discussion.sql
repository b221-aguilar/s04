-- Adds new records

-- add artists
INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");


-- add albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES
	("Fearless (Taylor's Version)", 
		"2021-04-09",
		3);

INSERT INTO albums (album_title, date_released, artist_id) VALUES
	("Red (Taylor's Version)", 
		"2021-11-12",
		3);

INSERT INTO albums (album_title, date_released, artist_id) VALUES
	("A Star is Born", 
		"2018-01-01",
		4);

INSERT INTO albums (album_title, date_released, artist_id) VALUES
	("An Evening with Silk Sonic", 
		"2021-11-12",
		7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES
	("Born This Way", 
		"2011-01-01",
		4);

INSERT INTO albums (album_title, date_released, artist_id) VALUES
	("24K Magic", 
		"2016-11-18",
		7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES
	("Purpose", 
		"2015-01-01",
		5);

INSERT INTO albums (album_title, date_released, artist_id) VALUES
	("Dangerous Woman", 
		"2016-01-01",
		6);


-- add songs
INSERT INTO songs (song_name, song_length, genre, album_id) VALUES (
		"All Too Well (Taylor's Version)",
		253,
		"Love Song",
		4	
	);

INSERT INTO songs (song_name, song_length, genre, album_id) VALUES (
		"Fearless",
		246,
		"Pop Rock",
		3
	);


INSERT INTO songs (song_name, song_length, genre, album_id) VALUES (
		"Black Eyes",
		151,
		"Rock",
		5	
	);

INSERT INTO songs (song_name, song_length, genre, album_id) VALUES (
		"Born This Way",
		252,
		"Pop",
		7
	);

INSERT INTO songs (song_name, song_length, genre, album_id) VALUES (
		"Sorry",
		212,
		"Pop Rock",
		9
	);

INSERT INTO songs (song_name, song_length, genre, album_id) VALUES (
		"Into You",
		242,
		"EDM",
		10
	);



-- Advanced Selected

-- Exclude records (NOT operator)
SELECT * FROM songs WHERE id != 5;

-- GT/GTE
SELECT * FROM songs WHERE id >= 7;

-- LT/LTE
SELECT * FROM songs WHERE id <= 7;

-- get the id (OR)
SELECT * FROM songs WHERE id = 5 OR id = 6 OR id = 10;

-- get the id (IN)
SELECT * FROM songs WHERE id IN (4,5,10);

SELECT * FROM songs WHERE id IN (5,7,10) AND song_name IN ("Into You", "Sorry");

-- find partial matches
SELECT * FROM songs WHERE song_name LIKE "%s";
	-- select a keyword/pattern where the strings ends with an "s"

SELECT * FROM songs WHERE song_name LIKE "b%";
	-- starts with a 'b'

SELECT * FROM songs WHERE song_name LIKE "%a%";
	-- any title with 'a'


-- Sort records
SELECT * FROM songs ORDER BY song_name ASC;
	-- ASC - Ascending
	-- DESC - Descending

-- get distinct records
SELECT DISTINCT genre FROM songs;
	-- shows all unqique values, removes duplicates

-- COUNT()
SELECT COUNT(*) FROM songs WHERE genre = "Pop Rock";
	-- gets number of entries given the specifications 

-- TABLE JOINS

-- Combine artists and albums table (JOIN/INNER JOIN)
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;


-- LEFT JOIN
SELECT * FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id;

-- RIGHT JOIN
SELECT * FROM artists
	RIGHT JOIN albums ON artists.id = albums.artist_id;


-- JOIN multiple tables
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;