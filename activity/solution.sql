-- 1.a
SELECT * FROM artists WHERE name LIKE "%d%";


-- 1.b
SELECT * FROM songs WHERE song_length < 230;


-- 1.c
SELECT album_title, song_name, song_length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- 1.d
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	WHERE album_title LIKE "%a%";

-- 1.e
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- 1.f
SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id
	ORDER BY album_title DESC;
	